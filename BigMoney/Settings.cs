﻿namespace BigMoney
{
    public static class Settings
    {
        public static string Company = "Game Tools";
        public static string Product = "Big Money";
        public static string ConfigFile = "config.json";        
    }
}
