﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;

namespace BigMoney
{
    public class NotifyPropertyChanged : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public NotifyPropertyChanged()
        {
            _syncCtx = SynchronizationContext.Current;
            if (_syncCtx == null)
            {
                _syncCtx = new SynchronizationContext();
            }
        }

        SynchronizationContext _syncCtx;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }

        public void OnPropertyChangedSync([CallerMemberName]string prop = "")
        {
            _syncCtx.Send((o) =>
            {
                try
                {
                    if (PropertyChanged != null)
                        PropertyChanged(this, new PropertyChangedEventArgs(prop));
                }
                catch
                { 
                }
            }, null);
        }
    }
}