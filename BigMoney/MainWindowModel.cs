﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace BigMoney
{
    public delegate void NotifyDelegate(string value);

    public class MainWindowModel : NotifyPropertyChanged
    {
        public event NotifyDelegate OnNotify;

        public MainWindowModel()
        {
            Values = new WealthGoal[]
            {
                new WealthGoal{ Value = 10000000 },
                new WealthGoal{ Value = 100000000 },
                new WealthGoal{ Value = 500000000 },
                new WealthGoal{ Value = 1000000000 },
                new WealthGoal{ Value = 1500000000 },
                new WealthGoal{ Value = 2000000000 }
            };

            BigValues = new WealthGoal[]
            {
                new WealthGoal{ Value = 10000000 },
                new WealthGoal{ Value = 100000000 },
                new WealthGoal{ Value = 500000000 },
                new WealthGoal{ Value = 1000000000 },
                new WealthGoal{ Value = 1500000000 },
                new WealthGoal{ Value = 2000000000 }
            };

            DefaultValues = new WealthGoal[]
            {
                new WealthGoal{ Value = 200000 },
                new WealthGoal{ Value = 500000 },
                new WealthGoal{ Value = 1000000 },
                new WealthGoal{ Value = 2000000 },
                new WealthGoal{ Value = 5000000 },
                new WealthGoal{ Value = 10000000 }
            };
        }

        internal bool CanPatch()
        {
            return _patches.Count == _filesToPatch.Length && _patches.All(o => o.Value.Offset != -1);
        }

        public WealthGoal[] Values { get; set; }

        private WealthGoal[] DefaultValues { get; set; }
        private WealthGoal[] BigValues { get; set; }

        public bool EnableBackup
        {
            get => _enableBackup;
            set
            {
                if (_enableBackup == value)
                    return;
                _enableBackup = value;
                OnPropertyChanged();
            }
        }

        public string GameFolder
        {
            get => _gameFolder;
            set
            {
                if (_gameFolder == value)
                    return;
                _gameFolder = value;
                OnPropertyChanged();
            }
        }

        public Dictionary<string, PatchInfo> Patches
        {
            get => _patches;
            set
            {
                _patches = value;
                OnPropertyChanged();
            }
        }

        public string PathToPortRoyale
        {
            get => _pathToPortRoyale;
            set
            {
                if (value == _pathToPortRoyale)
                    return;
                _pathToPortRoyale = value;
                GameFolder = Directory.GetParent(PathToPortRoyale).FullName;
                OnPropertyChanged();
            }
        }

        private void FindPatchOffset()
        {
            if (string.IsNullOrEmpty(PathToPortRoyale))
                return;

            if (!File.Exists(PathToPortRoyale))
                return;

            foreach (var file in _filesToPatch)
            {
                var fullPath = Path.Combine(GameFolder, file);
                OnNotify?.Invoke($"Read {fullPath}...");                

                if (!File.Exists(fullPath))
                {
                    Patches[file] = new PatchInfo
                    {
                        FullPath = fullPath,
                        Message = "File not found"
                    };
                    continue;
                }

                int[] values = DefaultValues.Select(o => o.Value).ToArray();
                byte[] pattern = new byte[values.Length * sizeof(int)];
                Buffer.BlockCopy(values, 0, pattern, 0, pattern.Length);

                var size = new FileInfo(fullPath).Length;
                long offset = -1;
                long chunk = 1 * 1024 * 1024;
                var buffer = new byte[chunk];

                using (var stream = new FileStream(fullPath, FileMode.Open))
                {
                    long pos = 0;
                    while (pos < size)
                    {
                        stream.Position = pos;
                        var read = stream.Read(buffer, 0, buffer.Length);

                        for (int i = 0; i < read - pattern.Length; ++i)
                        {
                            bool found = true;

                            for (int j = 0; j < pattern.Length; ++j)
                            {
                                if (pattern[j] != buffer[i + j])
                                {
                                    found = false;
                                    break;
                                }
                            }

                            if (found)
                            {
                                offset = pos + i;
                                break;
                            }
                        }

                        if (offset != -1)
                        {
                            break;
                        }

                        pos += read;

                        OnNotify?.Invoke($"Analyze {file} {pos / 1024 / 1024}/{size / 1024 / 1024} Mb...");

                        if (pos == size)
                            break;

                        pos -= pattern.Length;

                    }
                }

                Patches[file] = new PatchInfo
                {
                    FullPath = fullPath,
                    Offset = offset,
                    Values = Values.Select(o => o.Value).ToArray(),
                };

                OnPropertyChangedSync(nameof(Patches));

                OnNotify?.Invoke($"Analyze complete.");
            }

            if (Patches.Count != _filesToPatch.Length || Patches.Any(o => o.Value.Offset == -1))
            {
                OnNotify?.Invoke($"Can't patch as required files were not found.");
                return;
            }
        }

        public void Restore()
        {
            Values = DefaultValues.ToArray();
            OnPropertyChanged(nameof(Values));
        }

        public void SetBigValues()
        {
            Values = BigValues.ToArray();
            OnPropertyChanged(nameof(Values));
        }

        public void Patch()
        {            
            if (!Patched && DefaultValues.Select(o=>o.Value).SequenceEqual(Values.Select(o=>o.Value)))
            {
                OnNotify?.Invoke("Already restored");
                return;
            }

            if (EnableBackup && !Patched)
            {
                BackupOriginalFiles();
            }

            if (Patches.Any(o => o.Value.Offset == -1))
            {
                OnNotify?.Invoke("Can't patch data files as at least one is not recognised");
                return;
            }

            foreach (var patch in Patches)
            {
                if (patch.Value.Offset == -1)
                    continue;

                var values = Values.Select(o => o.Value).ToArray();
                byte[] data = new byte[values.Length * sizeof(int)];
                Buffer.BlockCopy(values, 0, data, 0, data.Length);

                using (var stream = new FileStream(patch.Value.FullPath, FileMode.Open, FileAccess.ReadWrite))
                {
                    stream.Position = patch.Value.Offset;
                    stream.Write(data, 0, data.Length);
                }
            }

            if (Values.Select(o => o.Value).SequenceEqual(DefaultValues.Select(o => o.Value)))
            {
                Patched = false;
                RemoveBackupFiles();
                OnNotify?.Invoke("Port Royale 3 has been restored");
            }
            else
            {
                Patched = true;
                OnNotify?.Invoke("Port Royale 3 has been patched succesfully");
            }
        }

        private void RemoveBackupFiles()
        {
            foreach (var pair in Patches)
            {
                var patch = pair.Value;
                var backupFile = patch.FullPath + ".old";
                if (File.Exists(backupFile))
                {
                    OnNotify?.Invoke($"Delete {patch.FullPath}.old");
                    try
                    {
                        File.Delete(backupFile);
                    }
                    catch (Exception e)
                    {
                        OnNotify?.Invoke($"Failed to delete {patch.FullPath}.old file. Error {e.Message}");
                    }
                }
            }
        }

        private void BackupOriginalFiles()
        {
            foreach (var pair in Patches)
            {
                var patch = pair.Value;
                var name = pair.Key;
                OnNotify?.Invoke($"Backup {name} to {patch.FullPath}.old");
                var backupFile = patch.FullPath + ".old";
                if (!File.Exists(backupFile))
                {
                    try
                    {

                        File.Copy(patch.FullPath, backupFile);
                    }
                    catch (Exception e)
                    {
                        OnNotify?.Invoke($"Backup of {patch.FullPath} failed. Error {e.Message}");
                    }
                }
            }
        }

        public void Refresh()
        {
            if (Patches.Count == 0 || Patches.Any(o => o.Value.Offset == -1))
            {
                FindPatchOffset();
            }
        }

        public bool Patched
        {
            get => _patched;
            set
            {
                if (_patched == value)
                    return;
                _patched = value;
                OnPropertyChanged();
            }
        }

        private Dictionary<string, PatchInfo> _patches = new Dictionary<string, PatchInfo>();
        private string _gameFolder;
        private string _pathToPortRoyale = "Select path to Port Royale 3 executable";
        private string[] _filesToPatch = new string[] { "data0.fuk", "data.fuk" };
        private bool _enableBackup = true;
        private bool _patched;
    }
}

