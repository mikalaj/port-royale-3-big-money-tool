﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Windows;

namespace BigMoney
{

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            var dataModel = GetModel();
            DataContext = Model = new MainWindowViewModel(dataModel);
            Model.Refresh();
        }

        MainWindowViewModel Model { get; set; }

        private MainWindowModel GetModel()
        {
            var appData = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), Settings.Company, Settings.Product);
            if (!Directory.Exists(appData))
                return new MainWindowModel();

            var configPath = System.IO.Path.Combine(appData, Settings.ConfigFile);
            if (!File.Exists(configPath))
                return new MainWindowModel();

            try
            {
                using (var reader = new JsonTextReader(File.OpenText(configPath)))
                {
                    return JsonSerializer.CreateDefault().Deserialize<MainWindowModel>(reader);
                }
            }
            catch(Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Application.Current.Shutdown();
                return new MainWindowModel();                
            }
        }
        
        private void Window_Closed(object sender, EventArgs e)
        {
            try
            {
                var appData = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), Settings.Company, Settings.Product);
                if (!Directory.Exists(appData))
                    Directory.CreateDirectory(appData);

                var configPath = System.IO.Path.Combine(appData, Settings.ConfigFile);

                using (var writer = new JsonTextWriter(File.CreateText(configPath)))
                {
                    writer.Formatting = Formatting.Indented;
                    JsonSerializer.CreateDefault().Serialize(writer, Model.Model);
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}
