﻿using Microsoft.Win32;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

namespace BigMoney
{    
    public class MainWindowViewModel : NotifyPropertyChanged
    {
        public MainWindowModel Model { get; set; }
        public MainWindowViewModel(MainWindowModel model)
        {
            _syncCtx = SynchronizationContext.Current;

            Model = model;
            Model.OnNotify += Model_OnNotify;    
            
            BrowseCommand = new Command(Browse);
            RestoreCommand = new Command(Restore);
            SetBigValuesCommand = new Command(SetBigValues);
            PatchCommand = new Command(Patch, CanPatch);            
            
            Refresh();
        }

        private SynchronizationContext _syncCtx;

        private bool CanPatch(object arg)
        {
            return File.Exists(Model.PathToPortRoyale) && Model.CanPatch();
        }

        private void Restore(object obj)
        {
            Model.Restore();
        }

        private void Model_OnNotify(string value)
        {
            CurrentTask = value;
            
            _syncCtx.Send((o) =>
            {
                try
                {
                    CommandManager.InvalidateRequerySuggested();
                }
                catch 
                {

                }
            }, null);
        }

        private void Patch(object obj)
        {
            Busy = true;
            Task.Run(() =>
            {
                Model.Patch();
            }).GetAwaiter().OnCompleted(() =>
            {
                Busy = false;
            });
        }

        public bool Busy
        {
            get => _busy;
            set
            {
                if (_busy == value)
                    return;
                _busy = value;
                OnPropertyChangedSync();
                CommandManager.InvalidateRequerySuggested();
            }
        }

        public void Refresh()
        {
            Busy = true;
            Task.Run(() =>
            {
                Model.Refresh();
            }).GetAwaiter().OnCompleted(() =>
            {
                Busy = false;
            });
        }

        private void SetBigValues(object obj)
        {
            Model.SetBigValues();                        
        }        

        public Command BrowseCommand { get; set; }
        public Command RestoreCommand { get; set; }
        public Command PatchCommand { get; set; }
        public Command SetBigValuesCommand { get; set; }        

        public void Browse(object o)
        {
            var openFileDialog = new OpenFileDialog();
            openFileDialog.FileName = Model.PathToPortRoyale;
            if (openFileDialog.ShowDialog() == true)
            {
                Model.PathToPortRoyale = openFileDialog.FileName;
                Refresh();
            }
            CommandManager.InvalidateRequerySuggested();
        }

        public string CurrentTask
        {
            get => _task;
            set
            {
                if (_task == value)
                    return;
                _task = value;
                OnPropertyChangedSync();
            }
        }
       
        public string Error
        {
            get => _error;
            set
            {
                if (_error == value)
                    return;
                _error = value;
                OnPropertyChangedSync();
            }
        }                

        private string _error;
        private string _task;
        private bool _busy;
    }

}

