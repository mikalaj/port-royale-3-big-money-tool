﻿namespace BigMoney
{
    public class PatchInfo
    {
        public bool Valid { get; set; } = false;
        public string FullPath { get; set; } = "";
        public long Offset { get; set; } = -1;
        public int[] Values { get; set; } = new int[0];
        public string Message { get; set; } = "";
    }
}

