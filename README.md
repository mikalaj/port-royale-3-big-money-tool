# Port Royale 3 Big Money Tool

Port Royale 3 tool to change wealth goals in local network game session.

## Application main window

![](BigMoney/Doc/app.png)

## Changes in game
When new game lan game is created select wealth goal and new target values
are displayed.

![](BigMoney/Doc/new_game.png)

When game is started main task shows selected target value.

![](BigMoney/Doc/tasks.png)
